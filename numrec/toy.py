from __future__ import division

import cv2
from math import sin, cos, pi
import numpy as np
import os.path as p
from matplotlib import pylab
from collections import deque


class Display(object):
    def __init__(self):
        self.images = deque()
        pylab.gray()

    def add(self, img, label=None):
        self.images.append((img, label))
        return self

    def show(self):
        if len(self.images) == 0:
            return
        elif len(self.images) == 1:
            img, label = self.images[0]
            pylab.imshow(img)
            if label:
                pylab.title(label)
        elif len(self.images) > 1:
            f = pylab.figure()
            for i, (img, label) in enumerate(self.images):
                a = f.add_subplot(len(self.images), 1, i + 1)
                if label:
                    a.set_title(label)
                pylab.imshow(img)
        pylab.show()


if __name__ == '__main__':
    def swt(canny, gd):
        w, h = canny.shape
        for y in range(h):
            for x in range(w):
                if not canny[x][y]:
                    continue

                trace_ray(canny, gd, x, y)

    def trace_ray(canny, gd, x, y):
        x0 = x
        y0 = y
        pass

    def ray_pixels(x, y, maxw, maxh, angle):
        cos_angle = cos(angle)
        sin_angle = sin(angle)
        x1 = x
        y1 = float(y)
        if 0 < abs(angle) < pi/4:
            while x1 < maxw:
                yield int(x1), int(y1)
                x1 += 1
                y1 += sin_angle


    # d = Display()
    # HERE = p.dirname(__file__)
    #
    # img = cv2.imread(p.join(HERE, 'out_number.jpg'), flags=0)
    # smooth = cv2.GaussianBlur(img, (5, 5), 0)
    # # d.add(smooth, 'smooth')
    #
    # dx = cv2.Sobel(smooth, cv2.CV_16S, dx=1, dy=0, ksize=0)
    # # d.add(dx, 'dx')
    #
    # dy = cv2.Sobel(img, cv2.CV_16S, dx=0, dy=1, ksize=3)
    # # d.add(dy, 'dy')
    #
    # canny = cv2.Canny(smooth, 200, 300)
    # # d.add(canny, 'canny')
    # gd = np.arctan2(dy, dx)
    # # print canny
    #
    # swt(canny, gd)
    # d.add(gd)
    # print dx.shape, dy.shape

    # d.show()

    img = np.zeros((15, 10))

    for x, y in ray_pixels(3, 3, 10, 15, pi/6):
        img[y][x] = 255

    img[0][0] = 120

    pylab.imshow(img, cmap='gray')
    pylab.show()
